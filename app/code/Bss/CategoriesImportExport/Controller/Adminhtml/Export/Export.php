<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CategoriesImportExport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CategoriesImportExport\Controller\Adminhtml\Export;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Magento\Framework\App\Response\Http\FileFactory;

/**
 * Class Export
 *
 * @package Bss\CategoriesImportExport\Controller\Adminhtml\Export
 */
class Export extends Action
{
    /**
     * @var \Bss\CategoriesImportExport\Model\ResourceModel\Export
     */
    protected $export;

    /**
     * @var string
     */
    protected $varDirectory;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $filesystem;

    /**
     * @var \Magento\Framework\Filesystem\Io\File
     */
    protected $io;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $datetime;

    /**
     * @var \Magento\Framework\File\Csv
     */
    protected $csv;

    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    /**
     * Export constructor.
     * @param Context $context
     * @param \Bss\CategoriesImportExport\Model\ResourceModel\Export $export
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $datetime
     * @param \Magento\Framework\Filesystem\Io\File $io
     * @param \Magento\Framework\File\Csv $csv
     * @param FileFactory $fileFactory
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     */
    public function __construct(
        Context $context,
        \Bss\CategoriesImportExport\Model\ResourceModel\Export $export,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Stdlib\DateTime\DateTime $datetime,
        \Magento\Framework\Filesystem\Io\File $io,
        \Magento\Framework\File\Csv $csv,
        FileFactory $fileFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
    ) {
        $this->export = $export;
        $this->filesystem = $filesystem;
        $this->datetime = $datetime;
        $this->io = $io;
        $this->csv = $csv;
        $this->fileFactory = $fileFactory;
        $this->resultRawFactory = $resultRawFactory;
        parent::__construct($context);
    }

    /**
     * Execute
     *
     * @return $this|\Magento\Framework\Controller\Result\Redirect | $this|\Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {
        try {
            $requestData = $this->getRequest()->getParams();
            $this->varDirectory = $this->filesystem
                ->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR);
            $dir = $this->varDirectory->getAbsolutePath('bss/export');
            $this->io->mkdir($dir, 0775);
            $currentDate = $this->export->formatDate($this->datetime->date());
            $outputFile = $dir."/Bss_Categories" . $currentDate . ".csv";
            $fileName = "Bss_Categories" . $currentDate . ".csv";
            $categories = $this->export->getCategories($requestData);
            $data = $this->export->getExportData($categories, $requestData);
            $this->csv->saveData($outputFile, $data);
            $this->fileFactory->create(
                $fileName,
                [
                    'type'  => "filename",
                    'value' => "bss/export/".$fileName,
                    'rm'    => true,
                ],
                \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR,
                'text/csv',
                null
            );
            $resultRaw = $this->resultRawFactory->create();
            return $resultRaw;
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        return $this->resultRedirectFactory->create()->setPath(
            '*/*/index',
            ['_secure'=>$this->getRequest()->isSecure()]
        );
    }

    /**
     * Is allowed
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Bss_CategoriesImportExport::importexport_categories_export');
    }
}
