/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CategoriesImportExport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
define([
    "jquery"
], function ($) {
    "use strict";
    $.widget('import.ajax', {
        _create: function () {
            $.ajax({
                url : this.options.ajaxUrl,
                type : 'post',
                dataType : 'json',
                success : function (result) {

                }
            });
        },
    });

    $('#entity').change(function () {
        if ($('#entity').val()=='bss_category') {
            $('#basic_behavior_import_multiple_value_separator').val('|');
            $('.field-basic_behaviorfields_enclosure').hide();
            $('.field-basic_behavior__import_field_separator').hide();
            $('.field-import_images_file_dir').hide();
            $('.field-basic_behavior_import_empty_attribute_value_constant').hide();
            $('#bss-version').show();
            $('#bss-version').appendTo('.field-entity .admin__field-control.control .admin__field');
        } else {
            $('#bss-version').hide();
            $('#basic_behavior_import_multiple_value_separator').val(',');
        }
    });



    return $.import.ajax;
});
