/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CategoriesImportExport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
require([
    'jquery',
    'prototype'
], function ($) {
    $("#entity").change(function () {
        $('#export-by').css('margin-left', '30px');
        $('#export_filter_container').css('display','block');
    });

    $("#export-by").change(function () {
        switch ($('#export-by').val()) {
            case 'store-id':
                $('#category-id').css('display', 'none');
                $('#store-id').css('display', 'block');
                break;
            case 'category-id':
                $('#category-id').css('display', 'block');
                $('#store-id').css('display', 'none');
                break;
            default:
                $('#category-id').css('display', 'none');
                $('#store-id').css('display', 'none');
        }
    });

    $('entity').selectedIndex = 0;
});