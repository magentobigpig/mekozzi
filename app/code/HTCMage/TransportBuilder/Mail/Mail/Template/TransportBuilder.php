<?php
 
namespace HTCMage\TransportBuilder\Mail\Mail\Template;
 
class TransportBuilder extends \Magento\Framework\Mail\Template\TransportBuilder
{    

    public function prepareMessage()
    {        
        $template = $this->getTemplate();
        $content = $template->processTemplate();
        switch ($template->getType()) {
            case TemplateTypesInterface::TYPE_TEXT:
                $part['type'] = MimeInterface::TYPE_TEXT;
                break;

            case TemplateTypesInterface::TYPE_HTML:
                $part['type'] = MimeInterface::TYPE_HTML;
                break;

            default:
                throw new LocalizedException(
                    new Phrase('Unknown template type')
                );
        }
        $mimePart = $this->mimePartInterfaceFactory->create(['content' => $content]);
        $this->messageData['body'] = $this->mimeMessageInterfaceFactory->create(
            ['parts' => [$mimePart]]
        );

        $this->messageData['subject'] = html_entity_decode(
            (string)$template->getSubject(),
            ENT_QUOTES
        );
        $this->messageData['encoding'] = $mimePart->getCharset();
        $this->message = $this->emailMessageInterfaceFactory->create($this->messageData);

        return $this;
    }
}
?>