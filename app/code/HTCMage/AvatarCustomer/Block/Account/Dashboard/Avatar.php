<?php
namespace HTCMage\AvatarCustomer\Block\Account\Dashboard;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Model\CustomerFactory;
use Magento\Store\Model\StoreManagerInterface;

class Avatar extends \Magento\Framework\View\Element\Template
{

    protected $storeManager;

	/**
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    protected $currentCustomer;

    protected $customerFactory;

    public function __construct(
        StoreManagerInterface $storeManager,
        CustomerFactory $customerFactory,
    	\Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
    	parent::__construct($context, $data);
    	$this->currentCustomer = $currentCustomer;
        $this->customerFactory = $customerFactory;
        $this->storeManager = $storeManager;
    }

    public function getCustomer()
    {
        try {
            return $this->currentCustomer->getCustomer();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    public function getBaseUrlAvatar()
    {
        $mediaUrl = $this ->_storeManager-> getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA );
        return $mediaUrl . 'catalog/product/';
    }

    public function getAvatar()
    {
        $mediaUrl = $this ->_storeManager-> getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA );

        return $mediaUrl . 'catalog/product/' . $this->customerFactory->create()->load($this->getCustomer()->getId())->getAvatar();
    }
}
