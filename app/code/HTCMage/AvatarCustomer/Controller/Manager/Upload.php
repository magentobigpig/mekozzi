<?php
namespace HTCMage\AvatarCustomer\Controller\Manager;

use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\App\Action;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\CustomerFactory;

class Upload extends \Magento\Framework\App\Action\Action
{

    const NAME_OF_FIELD = 'file';

    /**
     * Date
     * @var DataTime
     */
    protected $date;

    /**
     * Helper config admin
     * @var ConfigAdmin
     */
    protected $helperConfigAdmin;

    /**
     * FormKey Validator
     * @var Validator
     */
    protected $formKeyValidator;

    /**
     * File system
     * @var \Magento\Framework\Filesystem
     */
    protected $fileSystem;

    /**
     * UploaderFactory
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $fileUploaderFactory;

    /**
     * HTTP
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * Message Manager interface
     * @var ManagerInterface
     */
    protected $messageManager;

    protected $customerRepositoryInterface;

    protected $customerFactory;

    public function __construct(
        Action\Context $context,
        Filesystem $fileSystem,
        UploaderFactory $fileUploaderFactory,
        Http $request,
        Validator $formKeyValidator,
        DateTime $date,
        CustomerRepositoryInterface $customerRepositoryInterface,
        CustomerFactory $customerFactory
    ) {
        parent::__construct($context);
        $this->fileSystem = $fileSystem;
        $this->fileUploaderFactory = $fileUploaderFactory;
        $this->request = $request;
        $this->messageManager = $context->getMessageManager();
        $this->formKeyValidator = $formKeyValidator;
        $this->date = $date;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->customerFactory = $customerFactory;
    }

    /**
     * Execute
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $resultData = [];
            $data = $this->getRequest()->getPostValue();
            $mediaDir = $this->fileSystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
            $target = $mediaDir->getAbsolutePath('/catalog/product/');
        
            $uploader = $this->fileUploaderFactory->create(['fileId' => self::NAME_OF_FIELD]);
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png',]);
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(false);
            $uploader->setAllowCreateFolders(true);
            $dataImage = $uploader->save($target);
            $emailCustomer = $data['email'];
            $customer = $this->customerFactory->create()->setWebsiteId(1)->loadByEmail($emailCustomer);
            $customer->setAvatar($dataImage['file']);
            
            $customer->save();
            $resultData = [
                'avatar' => $dataImage['file'],
                'status' => 200
            ];
        } catch (\Exception $exception) {
            $resultData = [
                'status' => 500,
                'message' => $exception->getMessage()
            ];
        }

        $resultJson = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        $resultJson->setData($resultData);
        return $resultJson;
    }
}
