define ([
    'jquery'
], function ($) {
    'use strict';
    $.widget('mage.avatarCustomer', {

        _create: function() {
            this._bind();
        },

        _bind: function() {
            var self = this;
            $(this.element).find('#file_avatar').on(self.options.triggerEvent, function() {
                self._ajaxSubmit();
            });
        },

        _ajaxSubmit: function() {
            var self = this;
            var fd = new FormData();
            var files = $('#file_avatar')[0].files[0];
            fd.append('file',files);
            fd.append('email', $('#email_customer').val());
            $.ajax({
                url: self.options.url,
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response){
                    if(response != 0){
                        var avatar = self.options.getBaseUrlAvatar + response.avatar;
                        $("#img_avatar").attr("src", avatar); 
                    }else{
                        alert('file not uploaded');
                    }
                },
            });
        }
    });

    return $.mage.avatarCustomer;
});