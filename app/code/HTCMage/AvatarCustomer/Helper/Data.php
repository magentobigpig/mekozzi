<?php
namespace HTCMage\AvatarCustomer\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Model\CustomerFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Helper\Context;

class Data extends AbstractHelper
{
    protected $storeManager;

    /**
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    protected $currentCustomer;

    protected $customerFactory;

    public function __construct(
        StoreManagerInterface $storeManager,
        CustomerFactory $customerFactory,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context);
        $this->currentCustomer = $currentCustomer;
        $this->customerFactory = $customerFactory;
        $this->storeManager = $storeManager;
    }

    public function getCustomer()
    {
        try {
            return $this->currentCustomer->getCustomer();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    public function getAvatar()
    {
        $mediaUrl = $this ->_storeManager-> getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA );

        return $mediaUrl . 'catalog/product/' . $this->customerFactory->create()->load($this->getCustomer()->getId())->getAvatar();
    }
}
