<?php
namespace HTCMage\AvatarCustomer\Plugin;

use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Customer\Model\CustomerFactory;
use Magento\Store\Model\StoreManagerInterface;

class Avatar
{

    protected $storeManager;

    /**
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    protected $currentCustomer;

    /**
     * @var CurrentCustomer
     */
    protected $customerFactory;

    public function __construct(
        StoreManagerInterface $storeManager,
        CustomerFactory $customerFactory,
        CurrentCustomer $currentCustomer
    ) {
        $this->currentCustomer = $currentCustomer;
        $this->customerFactory = $customerFactory;
        $this->storeManager = $storeManager;
    }

    public function getCustomer()
    {
        try {
            return $this->currentCustomer->getCustomer();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    public function getAvatar()
    {
        $mediaUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA );

        return $mediaUrl . 'catalog/product/' . $this->customerFactory->create()->load($this->getCustomer()->getId())->getAvatar();
    }

    public function afterGetSectionData(
        $subject,
        $result
    ) {
        $result['avatar'] = $this->getAvatar();
        return $result;
    }
}
